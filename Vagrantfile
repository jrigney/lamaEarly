# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/xenial64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL

#  (1..3).each do |d|
#    config.vm.define "consul-#{d}" do |c|
#      c.vm.provision "shell", inline: "hostnamectl set-hostname consul-#{d}"
#      c.vm.provision "shell", inline: "apt-get update && apt-get dist-upgrade --yes && apt-get install build-essential --yes"
#      c.vm.provision "shell", inline: "cd /vagrant/consul && make ORD=#{d} deps install install-server"
#      c.vm.provision "shell", inline: "hostess add consul-#{d} $(</tmp/self.ip)"
#      config.vm.provider "virtualbox" do |v|
#        v.memory = 1024
#        v.cpus = 4
#      end
#    end
#  end

  config.vm.provision "shell", inline: "apt-get install python --yes"

  consulServerCount = 1
  (1..consulServerCount).each do |d|
    config.vm.define "consul-server-#{d}" do |c|

      c.vm.hostname="consul-server-#{d}"
      #the ip address needs to line up with consul-agent/templates/agent.json.j2 -> retry-join
      c.vm.network "private_network", ip: "172.20.20.1#{d}"
      if d == 1
        c.vm.network "forwarded_port", guest: 8500, host:8500
      end
      if d == consulServerCount
        c.vm.provision :ansible do |ansible|
          ansible.limit = "all"
          ansible.playbook = "provisioning/playbook.yml"
          ansible.verbose = 'vv'
          ansible.sudo = true
          ansible.host_vars = {
            "consul-server-#{d}" => {"host_ip" => "172.20.20.1#{d}",
                                    "consul_server_node_name" => "consul-server-#{d}"}
          }
          ansible.groups = {
            "consul-server" => ["consul-server-[1:#{consulServerCount}]"]
          }

        end
      end

      config.vm.provider "virtualbox" do |v|
        v.memory = 512
        v.cpus = 1
      end
    end
  end

  #consul agents
  consulAgentCount = 1
  (1..consulAgentCount).each do |d|
    config.vm.define "consul-agent-#{d}" do |c|

      c.vm.hostname="consul-agent-#{d}"
      c.vm.network "private_network", ip: "172.20.20.2#{d}"
      if d == consulAgentCount
        c.vm.provision :ansible do |ansible|
          ansible.limit = "all"
          ansible.playbook = "provisioning/playbook.yml"
          ansible.verbose = 'vv'
          ansible.sudo = true
          ansible.host_vars = {
            "consul-agent-#{d}" => {"host_ip" => "172.20.20.2#{d}",
                                    "consul_agent_node_name" => "consul-agent-#{d}"}
          }
          ansible.groups = {
            "consul-agent" => ["consul-agent-[1:#{consulAgentCount}]"]
          }
        end
      end

      config.vm.provider "virtualbox" do |v|
        v.memory = 1024
        v.cpus = 2
      end
    end
  end
end
